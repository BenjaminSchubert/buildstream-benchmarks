#!/usr/bin/env python3

import argparse
from contextlib import suppress
import datetime
import logging
import os
from pathlib import Path
import shutil
import subprocess
import sys
import tempfile
import time


ROOT = Path(__file__).resolve().parent
CACHE = ROOT.joinpath("cache")
BSTGEN_PATH = CACHE.joinpath("bstgen")
BUILDSTREAM_MASTER_PATH = CACHE.joinpath("buildstream/master")
BUILDSTREAM_TESTING_PATH = CACHE.joinpath("buildstream/testing")
PROJECTS_PATH = CACHE.joinpath("projects")

BSTGEN_REPO = "https://gitlab.com/BuildStream/benchmarks.git"
BUILDSTREAM_REPO = "https://gitlab.com/BuildStream/buildstream.git"

BSTGEN_BRANCH = "willsalmon/bstgen_moreshapes"

VENV_SETUP = CACHE.joinpath("venvs/setup")
VENV_MASTER = CACHE.joinpath("venvs/master")
VENV_TEST = CACHE.joinpath("venvs/test")

LOGGER = logging.getLogger()
LOGGER_FORMAT = "[%(asctime)s][%(levelname)s][%(name)s] %(message)s"
LOGGER_DIRECTORY = None


PROJECTS = [
    {
        "name": "fan-small",
        "parameters": ["fan", "-r", "2", "-l", "4"],
        "root": "build_all.bst",
    },
    {
        "name": "fan-medium",
        "parameters": ["fan", "-r", "3", "-l", "10"],
        "root": "build_all.bst",
    },
]


class SetupException(Exception):
    pass


def check_call(command, *, cwd=None, env=None):
    def get_log_path():
        log_filename = "__".join(command).replace("/", "_") + ".log"
        orig_log_path = str(LOGGER_DIRECTORY.joinpath(log_filename))
        log_path = orig_log_path + ".0"
        counter = 0

        while os.path.exists(log_path):
            counter += 1
            log_path = orig_log_path + "." + str(counter)

        return log_path

    if cwd is None:
        cwd = ROOT

    cwd = str(cwd)

    kwargs = {
        "universal_newlines": True,
        "stderr": subprocess.STDOUT,
        "cwd": cwd,
    }

    if env is not None:
        kwargs["env"] = env

    LOGGER.debug("Running '%s in '%s", " ".join(command), cwd)

    with open(get_log_path(), "w") as log_file:
        log_file.write(
            "Running {} in {}\n".format(" ".join(command), cwd)
        )
        return subprocess.check_call(command, **kwargs, stdout=log_file)


def parse_args(args = None):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--clean", action="store_true", help="Clean everything before running")

    parser.add_argument("--verbose", action="store_true")

    parser.add_argument("--no-setup", action="store_true", help="skip setup")

    parser.add_argument("--only", nargs="*", help="Only run selected projects")

    parser.add_argument("branch", help="Branch to compare against master")

    return parser.parse_args(args)


def clean():
    LOGGER.debug("Cleaning everything")

    with suppress(FileNotFoundError):
        shutil.rmtree(CACHE)


def update_git_repo(uri, dest, branch="master"):
    if not dest.exists():
        check_call(["git", "init", str(dest)])
        check_call(
            ["git", "remote", "add", "origin", uri], cwd=dest)
        check_call(["git", "fetch"], cwd=dest)
        check_call(["git", "checkout", branch], cwd=dest)

    check_call(["git", "pull"], cwd=dest)


def create_venv(dest, name):
    if not dest.exists():
        check_call(["python3", "-m", "venv", "--prompt", name, str(dest)])
    else:
        LOGGER.debug(
            "Virtualenv %s at %s already exists. Skipping", name, str(dest))


def install_package(package, venv, editable=False):
    command = [f"{str(venv)}/bin/python", "-m", "pip", "install"]

    if editable:
        command.append("-e")

    check_call([*command, package])


def generate_projects():
    for project in PROJECTS:
        project_path = PROJECTS_PATH.joinpath(project["name"])

        if project_path.exists():
            LOGGER.debug(
                "Project %s at %s already exists, skipping",
                project["name"],
                project_path,
            )
            continue

        check_call(
            [
                f"{str(VENV_SETUP)}/bin/bstgen",
                *project["parameters"],
                str(project_path),
            ],
        )


def build_project(project_path, root, venv):
    command = [
        f"{venv}/bin/bst",
        "--on-error", "terminate",
        "--no-interactive",
        "--no-colors",
        "build",
        root,
    ]

    start = time.time()

    with tempfile.TemporaryDirectory() as tmpdir:
        env = os.environ.copy()
        env["XDG_CACHE_HOME"] = tmpdir
        check_call(
            command,
            cwd=project_path,
            env=env,
        )

    return time.time() - start


def benchmark(projects):
    for project in projects:
        project_path = PROJECTS_PATH.joinpath(project["name"])

        master_time = build_project(project_path, project["root"], VENV_MASTER)
        test_time = build_project(project_path, project["root"], VENV_TEST)

        LOGGER.info(
            "Master time: %f, Test time: %f, Difference: %f, Percent: %f%%",
            master_time,
            test_time,
            test_time - master_time,
            ((test_time - master_time) / master_time) * 100,
        )


def setup(test_branch, do_clean=False):
    if do_clean:
        clean()

    update_git_repo(
        BSTGEN_REPO, BSTGEN_PATH, branch=BSTGEN_BRANCH)
    update_git_repo(BUILDSTREAM_REPO, BUILDSTREAM_MASTER_PATH, "master")
    update_git_repo(BUILDSTREAM_REPO, BUILDSTREAM_TESTING_PATH, test_branch)

    create_venv(VENV_SETUP, "bst-benchmark-setup")
    create_venv(VENV_MASTER, "bst-benchmark-master")
    create_venv(VENV_TEST, "bst-benchmark-test")

    install_package(
        str(BSTGEN_PATH.joinpath("contrib/bstgen")),
        VENV_SETUP,
        editable=True,
    )
    install_package(str(BUILDSTREAM_MASTER_PATH), VENV_MASTER, editable=True)
    install_package(str(BUILDSTREAM_TESTING_PATH), VENV_TEST, editable=True)

    generate_projects()


def setup_logging(level):
    global LOGGER_DIRECTORY
    LOGGER_DIRECTORY = ROOT.joinpath(
        "logs",
        datetime.datetime.now().replace(microsecond=0).isoformat(),
    )
    LOGGER_DIRECTORY.mkdir(parents=True)

    console_formatter = logging.Formatter(LOGGER_FORMAT)
    file_formatter = logging.Formatter(LOGGER_FORMAT)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(console_formatter)
    console_handler.setLevel(level)

    file_handler = logging.FileHandler(LOGGER_DIRECTORY.joinpath("root.log"))
    file_handler.setFormatter(file_formatter)

    LOGGER.addHandler(console_handler)
    LOGGER.addHandler(file_handler)

    LOGGER.setLevel(0)

    LOGGER.info("Logs saved in %s", LOGGER_DIRECTORY)


def main():
    args = parse_args()

    setup_logging(logging.DEBUG if args.verbose else logging.INFO)

    if not args.no_setup:
        try:
            setup(args.branch, args.clean)
        except SetupException:
            return 1

    if args.only is None:
        projects = PROJECTS
    else:
        projects = [
            project for project in PROJECTS if project["name"] in args.only
        ]
    benchmark(projects)


if __name__ == "__main__":
    sys.exit(main())
